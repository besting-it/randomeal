# Randomeal

Randomeal is a small .NET standard library which implements a simplified genetic algorithm using randomization and self-adjusting simulated annealing for converging quickly to desired results.

It can be used for estimating result vectors for different mathematical problems where suitable fitness functions can be defined. Randomeal has been created to be a tool that requires only minimal configuration.
Most of the relevant parameters are calculated automatically. In most cases only the maximum number of iterations and a target accuracy have to be provided.

Licensed under the Apache License, Version 2.0.

Nuget link: https://www.nuget.org/packages/Randomeal/

## Example

In this example 2 coefficients of a line function ax + b = y should be estimated. The goal: find a = -2 and b = 6 (or values close to these results) using two sample points P1=(1, 4) and P2=(2, 2).
The implementation can also be found in the unit test. Only one interface has to be implemented where two functions need to be defined:

* int Compare(MutableVector a, MutableVector b): The actual fitness function. Compares two randomly guessed solutions. In this case the euclidean distance is used.
* bool IsValid(MutableVector vector, double accuracyDelta): A function that checks if the currently best result is accurate enough. This is effectivly an abort condition.

Example implementation of IFitnessFunction:
```csharp
/// <summary>
/// Definition of a fitness function which compares estimated coefficients
/// of a line function ax + b = y. The goal: find a = -2 and b = 6
/// </summary>
class LineFitness : IFitnessFunction
{
    // The sample set for y = -2x + 6
    private double[] X { get; set; } = { 1, 2 };
    private double[] Y { get; set; } = { 4, 2 };

    #region IFitnessFunction

    /// <summary>
    /// Compares result evaluation of two coefficient candidates using eucl. distance
    /// </summary>
    /// <param name="x">1st candidate</param>
    /// <param name="y">2nd candidate</param>
    /// <returns>Comparison value</returns>
    public int Compare(MutableVector a, MutableVector b)
    {
        double [] ay = Eval(a, X);
        double [] by = Eval(b, X);
        double da = GetEuclideanDistance(Y, ay);
        double db = GetEuclideanDistance(Y, by);
        if (da == db)
        {
            return 0;
        }
        return (da < db) ? -1 : 1;
    }

    /// <summary>
    /// Checks if the evaluation of the best solution so far is accurate enough (abort condition)
    /// </summary>
    /// <param name="vector">Coefficients</param>
    /// <param name="accuracyDelta">Delta value, as defined in the config</param>
    /// <returns>True, if accurate</returns>
    public bool IsValid(MutableVector vector, double accuracyDelta)
    {
        return GetEuclideanDistance(Y, Eval(vector, X)) <= accuracyDelta;
    }
    
    #endregion

    /// <summary>
    /// The actual evaluation of the line function using two sample sets
    /// </summary>
    /// <param name="vector">Coefficients</param>
    /// <param name="x">Input values (x1, x2)</param>
    /// <returns>Result values (y1, y2) </returns>
    private double [] Eval(MutableVector vector, double [] x)
    {
        var e = vector.Elements;
        double y1 = e[0].Value * x[0] + e[1].Value;
        double y2 = e[0].Value * x[1] + e[1].Value;
        return new [] { y1, y2 };
    }
    
    private double GetEuclideanDistance(double[] a, double[] b)
    {
        double sum = 0;
        for (int i = 0; i < a.Length; i++)
        {
            sum += Math.Pow(b[i] - a[i], 2);
        }
        return Math.Sqrt(sum);
    }  
}
```

The main part:
```csharp
// Construct algorithm instance and configuration for finding coefficients of 
// a line function y = -2x + 6, i.e. a close to -2 and b close to 6
var alg = new Randomeal<LineFitness>(2, -100, 100)  // Find 2 coefficients (between -100 and 100)
{
    FitnessFunction = new LineFitness(),
    Config = new RandomealConfig()  // Note: default population size is 10, default mutation rate 0.25 (min. 1 element)
    {
        MaxIterations = 500,  // Max. no. of iterations (default is 1000)
        AccuracyDelta = 000001,  // The desired accuracy (result may have a eucl. distance delta of 0.000001)                
    }
    // Line fitness function y = -2x + 6
};

// Try estimating a good result
var res = alg.Evaluate();

// Print out the estimated coefficients and no. of iterations
Console.WriteLine("Result coefficients : " + res);
Console.WriteLine("Iterations needed   : " + alg.ActualIterations);

// Check if result is valid, i.e. within desired accuracy delta
Console.WriteLine(alg.IsValidResult);
```

Output:
```
Result coefficients : [-2.0000013750532877, 6.000002112708677]
Iterations needed   : 342
True
```

Note: To yield the same results on repeated calls to alg.Evaluate() call alg.Reset().

## Start values and locking

Starting from a vector where some elements are already found, these can be excluded from being altered. In this case: 

```csharp
var baseA = alg.BaseVector.Elements.First();
baseA.Value = -2;
baseA.IsLocked = true;

// Try estimating a good result
var res = alg.Evaluate();
```

Output:
```
Result coefficients : [-2, 5.999999734595494]
Iterations needed   : 19
True
```

If only good, but not exact values are known, these can be locked and unlocked again, when a certain accuracy is reached.

```csharp
var baseA = alg.BaseVector.Elements.First();
baseA.Value = -1.99;
baseA.IsLocked = true;
baseA.UnlockAccuracy = 0.05;

// Try estimating a good result
var res = alg.Evaluate();
```

Output:
```
Result coefficients : [-1.9999988561840811, 5.999997939157972]
Iterations needed   : 217
True
```
