﻿/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
using System;
using System.Linq;
using System.Text;

namespace BestingIT.Randomeal
{
	/// <summary>
	/// Instances of this class represent an array of mutable values
	/// </summary>
	public class MutableVector
	{
        
		public MutableValue[] Elements { get; set; }
        
		public MutableVector(Random rnd, int count, double lowerBound = 0, double upperBound = 1) : this(rnd, new double[count], lowerBound, upperBound)
		{
			_rnd = rnd;
		}

		public MutableVector(Random rnd, double[] values, double lowerBound = 0, double upperBound = 1)
		{
			_rnd = rnd;
			Elements = new MutableValue[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				Elements[i] = new MutableValue(_rnd, values[i], lowerBound, upperBound, false, 0);
			}
		}

		public MutableVector(Random rnd, MutableVector vector)
		{
			_rnd = rnd;
			Elements = new MutableValue[vector.Elements.Length];
			int i = 0;
			foreach (var next in vector.Elements)
			{
				Elements[i++] = new MutableValue(_rnd, next.Value, next.LowerBound, next.UpperBound, next.IsLocked, next.UnlockAccuracy);
			}
		}

		/// <summary>
		/// Mutates vector
		/// </summary>
		/// <param name="rate">Mutation rate (0..1)</param>
		/// <param name="annealing">Annealing factor, higher values for less mutation impact</param>
		/// <param name="accuracy">The current accuracy</param>
		internal void MutateVector(double rate, double annealing, double accuracy)
		{
			var unlockedElements = Elements.Where(e => !e.IsLockedWithAccuracy(accuracy)).ToArray();
			int count = Math.Max((int)Math.Round(rate * unlockedElements.Length), 1);
			for (int i = 0; i < count; i++)
			{
				unlockedElements[_rnd.Next(0, unlockedElements.Length)].Mutate(annealing, accuracy);
			}
		}

		public double[] ToArray()
		{
			var values = new double[Elements.Length];
			int i = 0;
			foreach (var next in Elements)
			{
				values[i++] = next.Value;
			}
			return values;
		}

		public override string ToString()
		{
			var sb = new StringBuilder();
			foreach (var next in Elements)
			{
				sb.Append(next + ", ");
			}
			return "[" + sb.ToString(0, sb.Length - 2) + "]";
		}
		
		private readonly Random _rnd;
        
	}
}
