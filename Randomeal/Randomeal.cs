﻿/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace BestingIT.Randomeal
{
    /// <summary>
    /// Randomeal is a small .NET standard library which implements a simplified genetic algorithm using 
    /// randomization and self-adjusting simulated annealing for converging quickly to desired results.
    /// 
    /// It can be used for estimating result vectors for different mathematical problems where suitable 
    /// fitness functions can be defined.
    /// </summary>
    /// <typeparam name="T">Type of concrete fitness function</typeparam>
    public class Randomeal<T> where T : class, IFitnessFunction
    {
        /// <summary>
        /// Configuration instance
        /// </summary>
        public RandomealConfig Config { get; set; } = new RandomealConfig();
        
        /// <summary>
        /// The fitness function
        /// </summary>
        public T FitnessFunction { get; set; }

        /// <summary>
        /// The number of iterations needed
        /// </summary>
        public int ActualIterations { get; private set; }

        /// <summary>
        /// True, if result is valid
        /// </summary>
        public bool IsValidResult { get; private set; }

        /// <summary>
        /// The base vector
        /// </summary>
        public MutableVector BaseVector => _baseVector;

        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="count">The number of elements in the base vector</param>
        /// <param name="lowerBound">The lower bound</param>
        /// <param name="upperBound">The upper bound</param>
        public Randomeal(int count, double lowerBound = 0, double upperBound = 1)
        {
            _baseVector = new MutableVector(_rnd, count, lowerBound, upperBound);
        }

        /// <summary>
        /// Reset random number generator
        /// </summary>
        /// <param name="seed">The seed (optional).</param>
        public void Reset(int seed = 0)
        {
            _rnd = new Random(seed);
            if (_baseVector != null)
            {
                _baseVector = new MutableVector(_rnd, _baseVector);
            }
        }

        /// <summary>
        /// Runs the actual algorithm
        /// </summary>
        /// <returns>Best estimation</returns>
        public MutableVector Evaluate()
        {
            double currentAccuracy = _baseVector.Elements.Max(e => e.Range);
            double currentAnnealing = 1;
            double rate = Config.MutationRate;
            int count = Config.PopulationSize;
            var population = new List<MutableVector>(count);
            var best = new MutableVector(_rnd, _baseVector);
            for (int i = 0; i < Config.MaxIterations; i++)
            {
                UpdatePopulation(best, population, count, rate, currentAnnealing, currentAccuracy);
                population.Sort(FitnessFunction);
                best = population.First();
                if (FitnessFunction.IsValid(best, Config.AccuracyDelta))
                {
                    ActualIterations = i;
                    IsValidResult = true;
                    return best;
                }
                if (!FitnessFunction.IsValid(best, currentAccuracy)) continue;
                currentAccuracy *= 0.5;
                currentAnnealing *= 2;
            }
            ActualIterations = Config.MaxIterations;
            IsValidResult = false;
            return best;
        }

        private void UpdatePopulation(MutableVector baseVector, List<MutableVector> target, int count, double rate, double annealing, double accuracy)
        {
            target.Clear();
            for (int i = 0; i < count; i++)
            {
                var next = new MutableVector(_rnd, baseVector);
                next.MutateVector(rate, annealing, accuracy);
                target.Add(next);
            }
        }
        
        private Random _rnd = new Random(0);
        
        private MutableVector _baseVector; 

    }
}
