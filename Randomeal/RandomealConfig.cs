﻿/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace BestingIT.Randomeal
{

    /// <summary>
    /// Configuration values for the algorithm
    /// </summary>
    public class RandomealConfig
    {
        private const int DefaultMaxIterations = 1000;
        private const int DefaultPopulationSize = 10;

        private const double DefaultMutationRate = 0.25;
        private const double DefaultAccuracyDelta = 0.001;

        /// <summary>
        /// The maximum number of iterations to perform
        /// </summary>
        public int MaxIterations { get; set; } = DefaultMaxIterations;

        /// <summary>
        /// The initial and intermediate population size
        /// </summary>
        public int PopulationSize { get; set; } = DefaultPopulationSize;

        /// <summary>
        /// The mutation rate (0..1)
        /// </summary>
        public double MutationRate { get; set; } = DefaultMutationRate;

        /// <summary>
        /// The accuracy delta to be passed to the fitness function
        /// </summary>
        public double AccuracyDelta { get; set; } = DefaultAccuracyDelta;

    }
}
