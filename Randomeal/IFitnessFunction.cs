﻿/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
using System.Collections.Generic;

namespace BestingIT.Randomeal
{
    /// <summary>
    /// Interface for the fitness function which compares mutable vectors and asseses validity.
    /// </summary>
    public interface IFitnessFunction : IComparer<MutableVector>
    {
        /// <summary>
        /// Indicates how accurate the desired result has been achieved.
        /// </summary>
        /// <param name="vector">The vector to test</param>
        /// <param name="accuracyDelta">The delta the actual result may differ from the desired result</param>
        /// <returns>True, if result is acceptable</returns>
        bool IsValid(MutableVector vector, double accuracyDelta);
    }
}
