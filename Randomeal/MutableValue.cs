﻿/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
using System;
using System.Globalization;

namespace BestingIT.Randomeal
{
	/// <summary>
	/// Instances of this class represent a mutable value which can be randomized.
	/// The impact of randomization can be influenced by an annealing factor.
	/// </summary>
	public class MutableValue
	{
        
		public double LowerBound { get; set; }

		public double UpperBound { get; set; }

		public double Value { get; set; }

		public double Range => UpperBound - LowerBound;
		
		public bool IsLocked { get; set; }
		
		public double UnlockAccuracy { get; set; }

		public MutableValue(Random rnd, double value, double lowerBound, double upperBound, bool isLocked, double unlockAccuracy)
		{
			_rnd = rnd;
			Value = value;
			LowerBound = lowerBound;
			UpperBound = upperBound;
			IsLocked = isLocked;
			UnlockAccuracy = unlockAccuracy;
		}

		public MutableValue(Random rnd, double value)
		{
			_rnd = rnd;
			Value = value;
			LowerBound = DefaultMin;
			UpperBound = DefaultMax;
		}

		/// <summary>
		/// Return the current locked state.
		/// </summary>
		/// <param name="accuracy">The current accuracy</param>
		/// <returns>True if currently locked, false otherwise</returns>
		internal bool IsLockedWithAccuracy(double accuracy)
		{
			if (IsLocked && accuracy > UnlockAccuracy)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Mutate current value using an annealing rate
		/// </summary>
		/// <param name="annealing">Annealing factor, higher values means original value is less likely to be subjected to randomized changes.</param>
		/// <param name="accuracy"></param>
		internal void Mutate(double annealing, double accuracy)
		{
			if (IsLockedWithAccuracy(accuracy))
			{
				throw new InvalidOperationException("Element is currently locked and excluded from mutating.");
			}
			Clip();
			double range = (UpperBound - LowerBound) / annealing;
			Value += (_rnd.NextDouble() - 0.5) * range;			
            Clip();
		}

		private void Clip()
		{
			if (Value > UpperBound)
			{
				Value = UpperBound;
			}
			else if (Value < LowerBound)
			{
				Value = LowerBound;
			}
		}

		public override string ToString() => Value.ToString(CultureInfo.InvariantCulture);

		private readonly Random _rnd;
		private const int DefaultMax = 1;
		private const int DefaultMin = 0;
        
	}
}
