using BestingIT.Randomeal;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Linq;

namespace RandomealTest
{
    /// <summary>
    /// Definition of a fitness function which compares estimated coefficients
    /// of a line function ax + b = y. The goal: find a = -2 and b = 6
    /// </summary>
    class LineFitness : IFitnessFunction
    {
        // The sample set for y = -2x + 6
        private double[] X { get; } = { 1, 2 };
        private double[] Y { get; } = { 4, 2 };

        #region IFitnessFunction

        /// <summary>
        /// Compares result evaluation of two coefficient candidates using eucl. distance
        /// </summary>
        /// <param name="x">1st candidate</param>
        /// <param name="y">2nd candidate</param>
        /// <returns>Comparison value</returns>
        public int Compare(MutableVector a, MutableVector b)
        {
            double [] ay = Eval(a, X);
            double [] by = Eval(b, X);
            double da = GetEuclideanDistance(Y, ay);
            double db = GetEuclideanDistance(Y, by);
            if (da == db)
            {
                return 0;
            }
            return (da < db) ? -1 : 1;
        }

        /// <summary>
        /// Checks if the evaluation of the best solution so far is accurate enough (abort condition)
        /// </summary>
        /// <param name="vector">Coefficients</param>
        /// <param name="accuracyDelta">Delta value, as defined in the config</param>
        /// <returns>True, if accurate</returns>
        public bool IsValid(MutableVector vector, double accuracyDelta)
        {
            return GetEuclideanDistance(Y, Eval(vector, X)) <= accuracyDelta;
        }

        #endregion

        /// <summary>
        /// The actual evaluation of the line function using two sample sets
        /// </summary>
        /// <param name="vector">Coefficients</param>
        /// <param name="x">Input values (x1, x2)</param>
        /// <returns>Result values (y1, y2) </returns>
        private double [] Eval(MutableVector vector, double [] x)
        {
            var e = vector.Elements;
            double y1 = e[0].Value * x[0] + e[1].Value;
            double y2 = e[0].Value * x[1] + e[1].Value;
            return new [] {y1, y2};
        }

        private double GetEuclideanDistance(double[] a, double[] b)
        {
            double sum = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sum += Math.Pow(b[i] - a[i], 2);
            }
            return Math.Sqrt(sum);
        }
    }

    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestRandomeal()
        {
            // Construct algorithm instance and configuration for finding coefficients of 
            // a line function y = -2x + 6, i.e. a close to -2 and b close to 6
            var alg = new Randomeal<LineFitness>(2, -100, 100)  // Find 2 coefficients (between -100 and 100)
            {
                FitnessFunction = new LineFitness(),
                Config = new RandomealConfig()  // Note: default population size is 10, default mutation rate 0.25 (min. 1 element)
                {
                    MaxIterations = 500,  // Max. no. of iterations (default is 1000)
                    AccuracyDelta = 0.000001,  // The desired accuracy (result may have a eucl. distance delta of 0.01)                
                }
                // Line fitness function y = -2x + 6
            };

            // var baseA = alg.BaseVector.Elements.First();
            // baseA.Value = -2;
            // baseA.IsLocked = true;
            
            // var baseA = alg.BaseVector.Elements.First();
            // baseA.Value = -1.99;
            // baseA.IsLocked = true;
            // baseA.UnlockAccuracy = 0.05;

            // Try estimating a good result
            var res = alg.Evaluate();

            int i = alg.ActualIterations;
            
            // Print out the estimated coefficients and no. of iterations
            Debug.WriteLine("Result coefficients : " + res);
            Debug.WriteLine("Iterations needed   : " + i);

            // Check if result is valid, i.e. within desired accuracy delta
            Assert.IsTrue(alg.IsValidResult);
            var a = res.Elements[0].Value;
            var b = res.Elements[1].Value;
            Assert.AreEqual(-2, a, 0.1);
            Assert.AreEqual(6, b, 0.1);

            // Check if we yield the same results after reset
            alg.Reset();
            res = alg.Evaluate();

            Assert.AreEqual(a, res.Elements[0].Value, 0.000001);
            Assert.AreEqual(b, res.Elements[1].Value, 0.000001);   
            Assert.AreEqual(i, alg.ActualIterations);            
        }
    }
}
